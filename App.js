import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import thunk from 'redux-thunk';
import {applyMiddleware, createStore} from "redux";
import axios from 'axios';

import {Provider} from "react-redux";
import reducer from './store/reducer';
import Contacts from './containers/Contacts/Contacts';

axios.defaults.baseURL = 'https://getpizzaapp.firebaseio.com';

export default class App extends React.Component {

    render() {
    const store = createStore(reducer, applyMiddleware(thunk));
        return (
            <Provider store={store}>
                <Contacts/>
            </Provider>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
});
