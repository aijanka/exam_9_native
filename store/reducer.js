import * as actionTypes from './actionTypes';

const initialState = {
    contacts: []
};

const reducer = (state = initialState, action) => {
    switch(action.type) {
        case actionTypes.CONTACTS_REQUEST:
            return {...state, loading: true};
        case actionTypes.CONTACTS_SUCCESS:
            return {...state, loading: false, contacts: action.contacts};
        case actionTypes.CONTACTS_FAILURE:
            return {...state, loading: false, error: action.error};
        default :
            return state;
    }
};

export default reducer;