import axios from 'axios';
import * as actionTypes from './actionTypes';

export const contactsRequest = () => ({type: actionTypes.CONTACTS_REQUEST});
export const contactsSuccess = contacts => ({type: actionTypes.CONTACTS_SUCCESS, contacts});
export const contactsFailure = error => ({type: actionTypes.CONTACTS_FAILURE, error});

export const loadContacts = () => {
    return (dispatch, getState) => {
        dispatch(contactsRequest());
        axios.get('/contacts.json').then(response => {
            dispatch(contactsSuccess(response.data));
        }, error => {
            dispatch(contactsFailure(error));
        })
    }
};