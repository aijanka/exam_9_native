import React from 'react';
import {FlatList, Image, Modal, ScrollView, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import {loadContacts} from "../../store/actions";
import Contact from "../../components/Contact/Contact";
import {connect} from "react-redux";

class Contacts extends React.Component {
    state = {
        showModal: false,
        currentKey: ''
    }

    componentDidMount() {
        this.props.loadContacts();
    }

    toggleModal = () => {
        this.setState(prevState => ({showModal: !prevState.showModal}));
    }

    showFull = (event, key) => {
        this.setState({currentKey: key});
        this.toggleModal();
    };

    render() {
        let keys = this.props.contacts ? Object.keys(this.props.contacts) : [];
        const c = this.props.contacts;
        // let other = keys.map(key => ({...c[key], key}));
        // console.log(other);
        return (
            <View>
                {this.state.currentKey ? (
                    <Modal
                        visible={this.state.showModal}
                        onRequestClose={this.toggleModal}
                        animationType="slide"
                        transparent={false}
                    >
                        <View style={styles.contact}>

                            <Image style={{width: 200, height: 200, marginBottom: 20}}
                                   source={{uri: c[this.state.currentKey].img}}/>
                            <View>
                                <Text>Name: {c[this.state.currentKey].name}</Text>
                                <Text>Phone number: {c[this.state.currentKey].number}</Text>
                                <Text>Email: {c[this.state.currentKey].email}</Text>
                                <TouchableOpacity onPress={this.toggleModal} style={styles.closeButton}><Text>Back to
                                    contacts</Text></TouchableOpacity>
                            </View>
                        </View>
                    </Modal>) : null}

                {/*<FlatList*/}
                    {/*data={other}*/}
                    {/*renderItem={(otherItem) => (*/}
                        {/*<Contact*/}
                            {/*name={otherItem.name}*/}
                            {/*number={otherItem.number}*/}
                            {/*email={otherItem.email}*/}
                            {/*img={otherItem.img}*/}
                            {/*showFull={(event) => this.showFull(event, otherItem.key)}*/}
                        {/*/>*/}
                    {/*)}*/}
                {/*/>*/}

                <ScrollView style={styles.contacts}>
                    {keys.map(key => {
                        return (
                            <Contact
                                key={key}
                                name={c[key].name}
                                number={c[key].number}
                                email={c[key].email}
                                img={c[key].img}
                                showFull={(event) => this.showFull(event, key)}
                            />
                        )
                    })}
                </ScrollView>
            </View>

        )
    }
}

const mapStateToProps = state => ({
    contacts: state.contacts
});

const mapDispatchToProps = dispatch => ({
    loadContacts: () => dispatch(loadContacts()),
});

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
    contacts: {
        margin: 20
    },
    contact: {
        marginTop: 50,
        justifyContent: 'center',
        alignItems: 'center',
        flexWrap: 'wrap'
    },
    closeButton: {
        justifyContent: 'center',
        borderColor: 'black',
        borderWidth: 2,
        padding: 6,
        borderRadius: 15,
        marginTop: 20
    }
});

export default connect(mapStateToProps, mapDispatchToProps)(Contacts);