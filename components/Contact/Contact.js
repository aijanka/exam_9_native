import React from 'react';
import {Image, Text, View, StyleSheet, TouchableOpacity} from "react-native";

const Contact = props => {
   return (
       <TouchableOpacity onPress={props.showFull} style={styles.contact}>
           <Image style={{width: 66, height: 58}} source={{uri: props.img}}/>
           <View>
               <Text>Name: {props.name}</Text>
               <Text>Phone number: {props.number}</Text>
               <Text>Email: {props.email}</Text>
           </View>
       </TouchableOpacity>
   )
};

const styles = StyleSheet.create({
    contact: {
        flexDirection: 'row',
        justifyContent: 'space-around',
        marginTop: 20
    }
})

export default Contact;